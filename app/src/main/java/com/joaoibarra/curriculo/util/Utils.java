package com.joaoibarra.curriculo.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import com.joaoibarra.curriculo.R;

public class Utils {

    public static int getToolbarHeight(Context context) {
        final TypedArray styledAttributes = context.getTheme().obtainStyledAttributes(
                new int[]{R.attr.actionBarSize});
        int toolbarHeight = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        return toolbarHeight;
    }

    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public int currentOrientation(Context context){
        //1.portrait 2.landscape
        return context.getResources().getConfiguration().orientation;
    }

    public int getColumnNumbers(Context context){
        if(isTablet(context) && currentOrientation(context) == 1){
            return 2;
        }else if(isTablet(context) && currentOrientation(context) == 2){
            return 3;
        }else if(!isTablet(context) && currentOrientation(context) == 1){
            return 1;
        }else if(!isTablet(context) && currentOrientation(context) == 2){
            return 2;
        }else
            return 2;
    }

}