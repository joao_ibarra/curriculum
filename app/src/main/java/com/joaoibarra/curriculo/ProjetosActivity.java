package com.joaoibarra.curriculo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import com.joaoibarra.curriculo.fragments.ProjetoFragment;
import com.joaoibarra.curriculo.util.SlidingTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/25/15.
 */
public class ProjetosActivity extends AppCompatActivity {
    Toolbar toolbar;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        id = intent.getIntExtra("ID", 0);
        switch (id){
            case 0:
                setTheme(R.style.CustomThemeTeal);
                break;
            case 1:
                setTheme(R.style.CustomThemeBlue);
                break;
            case 2:
                setTheme(R.style.CustomThemeBlueGrey);
                break;
            case 3:
                setTheme(R.style.CustomThemeCyan);
                break;
        }

        setContentView(R.layout.activity_projetos);


        initToolbar();
        initViewPagerAndTabs();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.app_name));
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            TypedValue typedValue = new TypedValue();
            getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
            final  int color = typedValue.data;
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(color);
            getWindow().setNavigationBarColor(color);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initViewPagerAndTabs() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        ProjetoFragment projetoFragment = new ProjetoFragment();
        projetoFragment.setArguments(getIntent().getExtras());
        switch (id){
            case 0:
                pagerAdapter.addFragment(projetoFragment.createInstance(0), "Eletrobrás");
                pagerAdapter.addFragment(projetoFragment.createInstance(1), "CELPE");
                pagerAdapter.addFragment(projetoFragment.createInstance(2), "CPFL");
                pagerAdapter.addFragment(projetoFragment.createInstance(3), "PMESP");
                pagerAdapter.addFragment(projetoFragment.createInstance(4), "Desing/ Webdesign");
                break;
            case 1:
                pagerAdapter.addFragment(projetoFragment.createInstance(5), "Gerente Boviplan");
                pagerAdapter.addFragment(projetoFragment.createInstance(6), "Mercearia");
                pagerAdapter.addFragment(projetoFragment.createInstance(7), "Capins");
                pagerAdapter.addFragment(projetoFragment.createInstance(8), "Scot Consultoria");
                break;
            case 2:
                pagerAdapter.addFragment(projetoFragment.createInstance(9), "IPAM");
                pagerAdapter.addFragment(projetoFragment.createInstance(10), "Luigi Bertolli");
                pagerAdapter.addFragment(projetoFragment.createInstance(11), "Santa Graça");
                pagerAdapter.addFragment(projetoFragment.createInstance(12), "A Paulistinha");
                pagerAdapter.addFragment(projetoFragment.createInstance(13), "Akad");
                break;
            case 3:
                pagerAdapter.addFragment(projetoFragment.createInstance(14), "Smartfactor");
                pagerAdapter.addFragment(projetoFragment.createInstance(15), "Estudos");
                break;
            case 4:
                pagerAdapter.addFragment(projetoFragment.createInstance(16), "CG 115 anos");
                pagerAdapter.addFragment(projetoFragment.createInstance(17), "Postal Saúde");
                break;
        }

        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        tabLayout.setHorizontalScrollBarEnabled(true);
        tabLayout.setSelectedIndicatorColors(R.color.primary_dark);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
    }

    static class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}
