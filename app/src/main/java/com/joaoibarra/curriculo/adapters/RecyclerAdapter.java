package com.joaoibarra.curriculo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.viewholder.RecyclerItemViewHolder;
import com.joaoibarra.curriculo.models.ProjetoInfo;

import java.util.List;

/**
 * Created by joaoibarra on 6/25/15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ProjetoInfo> mItemList;

    public RecyclerAdapter(List<ProjetoInfo> itemList) {
        mItemList = itemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.card_projeto_header, parent, false);
        return new ProjetoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ProjetoViewHolder holder = (ProjetoViewHolder) viewHolder;
        ProjetoInfo ci = mItemList.get(i);
        holder.titulo.setText(ci.getTitulo());
        holder.subtitulo.setText(ci.getSubtitulo());
        holder.descricao.setText(ci.getDescricao());
        holder.foto.setImageDrawable(ci.getFoto());
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public class ProjetoViewHolder extends RecyclerView.ViewHolder {

        protected TextView titulo;
        protected TextView subtitulo;
        protected TextView descricao;
        protected ImageView foto;
        protected Button mais;

        public ProjetoViewHolder(View v) {
            super(v);
            titulo =  (TextView) v.findViewById(R.id.titulo);
            subtitulo = (TextView)  v.findViewById(R.id.subtitulo);
            descricao = (TextView)  v.findViewById(R.id.descricao);
            foto = (ImageView) v.findViewById(R.id.foto);
        /*mais = (Button) v.findViewById(R.id.mais);*/
        }


    }

}