package com.joaoibarra.curriculo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.models.SobreInfo;

import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */

public class PartidaAdapter extends RecyclerView.Adapter<PartidaAdapter.ContactViewHolder> {

    private List<SobreInfo> contactList;

    public PartidaAdapter(List<SobreInfo> contactList) {
        this.contactList = contactList;
    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        SobreInfo ci = contactList.get(i);
        /*contactViewHolder.vName.setText(ci.getName() + " " + ci.getSurname());
        contactViewHolder.vSurname.setText(ci.getName() + " " + ci.getSurname());
        contactViewHolder.vEmail.setText(ci.getName() + " " + ci.getSurname());
        contactViewHolder.vAddress.setText(ci.getName() + " " + ci.getSurname());*/
        /*contactViewHolder.vTitle.setText(ci.getName() + " " + ci.getSurname());*/
        //contactViewHolder.vPosition.setText("" + (i+1) );
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_partida, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView vPontuacao1;
        protected TextView vPontuacao2;
        protected TextView vName;
        protected TextView vSurname;
        protected TextView vEmail;
        protected TextView vTitle;
        protected TextView vAddress;

        public ContactViewHolder(View v) {
            super(v);
            vPontuacao1 =  (TextView) v.findViewById(R.id.pontuacao_1);
            vPontuacao2 =  (TextView) v.findViewById(R.id.pontuacao_2);
            vName =  (TextView) v.findViewById(R.id.txtName);
            vSurname = (TextView)  v.findViewById(R.id.txtSurname);
            vEmail = (TextView)  v.findViewById(R.id.txtEmail);
            vTitle = (TextView) v.findViewById(R.id.title);
            vAddress = (TextView) v.findViewById(R.id.txtAdd);
        }
    }
}