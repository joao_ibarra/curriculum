package com.joaoibarra.curriculo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.models.SobreInfo;

import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */

public class SobreAdapter extends RecyclerView.Adapter<SobreAdapter.SobreViewHolder> {

    private List<SobreInfo> contactList;

    public SobreAdapter(List<SobreInfo> contactList) {
        this.contactList = contactList;
    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(SobreViewHolder contactViewHolder, int i) {
        SobreInfo ci = contactList.get(i);
        contactViewHolder.titulo.setText(ci.getTitulo());
        contactViewHolder.info1.setText(ci.getInfo1());
        contactViewHolder.info2.setText(ci.getInfo2());
        contactViewHolder.foto.setImageDrawable(ci.getFoto());
    }

    @Override
    public SobreViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_sobre, viewGroup, false);

        return new SobreViewHolder(itemView);
    }

    public static class SobreViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        protected TextView titulo;
        protected TextView info1;
        protected TextView info2;
        protected ImageView foto;

        public SobreViewHolder(View v) {
            super(v);
            titulo =  (TextView) v.findViewById(R.id.titulo);
            info1 = (TextView)  v.findViewById(R.id.info1);
            info2 = (TextView)  v.findViewById(R.id.info2);
            foto = (ImageView) v.findViewById(R.id.foto);
        }

        @Override
        public void onClick(View v) {

        }
    }
}