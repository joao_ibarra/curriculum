package com.joaoibarra.curriculo.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.models.SobreInfo;

import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */

public class ClassificacaoAdapter extends RecyclerView.Adapter<ClassificacaoAdapter.ContactViewHolder> {

    private List<SobreInfo> contactList;

    public ClassificacaoAdapter(List<SobreInfo> contactList) {
        this.contactList = contactList;
    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        SobreInfo ci = contactList.get(i);
       /* contactViewHolder.vName.setText(ci.getName());
        contactViewHolder.vSurname.setText(ci.getSurname());*/
        /*contactViewHolder.vEmail.setText(ci.getEmail());
        contactViewHolder.vTitle.setText(ci.getName() + " " + ci.getSurname());*/
        contactViewHolder.vPosition.setText("" + (i+1) );
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_classificacao, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView vPosition;
        protected TextView vName;
        protected TextView vSurname;
        protected TextView vEmail;
        protected TextView vTitle;

        public ContactViewHolder(View v) {
            super(v);
            vPosition =  (TextView) v.findViewById(R.id.posicao);
            vName =  (TextView) v.findViewById(R.id.txtName);
            vSurname = (TextView)  v.findViewById(R.id.txtSurname);
            vEmail = (TextView)  v.findViewById(R.id.txtEmail);
            vTitle = (TextView) v.findViewById(R.id.title);
        }
    }
}