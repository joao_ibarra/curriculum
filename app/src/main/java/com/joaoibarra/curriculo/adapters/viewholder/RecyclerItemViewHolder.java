package com.joaoibarra.curriculo.adapters.viewholder;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.joaoibarra.curriculo.R;

/**
 * Created by joaoibarra on 6/25/15.
 */
public class RecyclerItemViewHolder extends RecyclerView.ViewHolder {

    protected TextView titulo;
    protected TextView subtitulo;
    protected TextView descricao;
    protected ImageView foto;
    protected Button mais;

   /* public RecyclerItemViewHolder(final View parent, TextView itemTextView) {
        super(parent);
        mItemTextView = itemTextView;
    }*/

    public RecyclerItemViewHolder(View v) {
        super(v);
        titulo =  (TextView) v.findViewById(R.id.titulo);
        subtitulo = (TextView)  v.findViewById(R.id.subtitulo);
        descricao = (TextView)  v.findViewById(R.id.descricao);
        foto = (ImageView) v.findViewById(R.id.foto);
        /*mais = (Button) v.findViewById(R.id.mais);*/
    }

   /* public static RecyclerItemViewHolder newInstance(View parent) {
        TextView itemTextView = (TextView) parent.findViewById(R.id.titulo);
        return new RecyclerItemViewHolder(parent, itemTextView);
    }

    public void setItemText(CharSequence text) {
        mItemTextView.setText(text);
    }*/

}
