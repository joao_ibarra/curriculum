package com.joaoibarra.curriculo.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.joaoibarra.curriculo.ProjetosActivity;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.models.ExperienciaInfo;
import com.joaoibarra.curriculo.models.SobreInfo;

import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */

public class ExperienciaAdapter extends RecyclerView.Adapter<ExperienciaAdapter.ExperienciaViewHolder> {

    private List<ExperienciaInfo> contactList;
    private Context context;

    public ExperienciaAdapter(List<ExperienciaInfo> contactList, Context context) {
        this.contactList = contactList;
        this.context = context;
    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ExperienciaViewHolder contactViewHolder, final int i) {
        ExperienciaInfo ci = contactList.get(i);
        contactViewHolder.titulo.setText(ci.getTitulo());
        contactViewHolder.info1.setText(ci.getInfo1());
        contactViewHolder.info2.setText(ci.getInfo2());
        contactViewHolder.info3.setText(ci.getInfo3());
        contactViewHolder.foto.setImageDrawable(ci.getFoto());
        contactViewHolder.mais.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent projetos = new Intent(context, ProjetosActivity.class);
                Bundle extras = new Bundle();
                extras.putInt("ID",i);
                extras.putString("EXTRA_PASSWORD","my_password");
                projetos.putExtras(extras);
                context.startActivity(projetos);
            }
        });
    }

    @Override
    public ExperienciaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_experiencia, viewGroup, false);

        return new ExperienciaViewHolder(itemView);
    }

    public static class ExperienciaViewHolder extends RecyclerView.ViewHolder /*implements View.OnClickListener*/{

        protected TextView titulo;
        protected TextView info1;
        protected TextView info2;
        protected TextView info3;
        protected ImageView foto;
        protected Button mais;

        public ExperienciaViewHolder(View v) {
            super(v);
            titulo =  (TextView) v.findViewById(R.id.titulo);
            info1 = (TextView)  v.findViewById(R.id.info1);
            info2 = (TextView)  v.findViewById(R.id.info2);
            info3 = (TextView)  v.findViewById(R.id.info3);
            foto = (ImageView) v.findViewById(R.id.foto);
            mais = (Button) v.findViewById(R.id.mais);
        }



      /*  @Override
        public void onClick(View v) {
           *//* FragmentManager fragmentManager;
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, new PartidaFragment())
                    .addToBackStack("bets")
                    .commit();*//*
        }*/

        /*@Override
        public void onClick(View v) {
            if (v instanceof Imageview){
                mListener.onTomato((ImageView)v)
            } else {
                mListener.onPotato(v);
            }
        }

        public static interface IMyViewHolderClicks {
            public void onPotato(View caller);
            public void onTomato(ImageView callerImage);
        }*/
    }
}