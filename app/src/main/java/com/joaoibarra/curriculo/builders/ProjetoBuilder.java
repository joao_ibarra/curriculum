package com.joaoibarra.curriculo.builders;

import android.graphics.drawable.Drawable;

import com.joaoibarra.curriculo.models.ProjetoInfo;

/**
 * Created by joaoibarra on 11/4/15.
 */
public class ProjetoBuilder {
    private String titulo;
    private String subtitulo;
    private String descricao;
    private Drawable foto;
    public ProjetoBuilder(){

    }

    public ProjetoBuilder setTitulo(String titulo) {
        this.titulo = titulo;
        return this;
    }

    public ProjetoBuilder setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
        return this;
    }

    public ProjetoBuilder setDescricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public ProjetoBuilder setFoto(Drawable foto) {
        this.foto = foto;
        return this;
    }

    public ProjetoInfo createProjetoInfo(){
        return new ProjetoInfo(titulo, subtitulo, descricao, foto);
    }
}
