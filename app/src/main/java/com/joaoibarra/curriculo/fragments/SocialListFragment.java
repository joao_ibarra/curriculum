package com.joaoibarra.curriculo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.SocialAdapter;
import com.joaoibarra.curriculo.models.SobreInfo;
import com.joaoibarra.curriculo.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */
public class SocialListFragment extends Fragment {
    public SocialListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_list_classificacao, container, false);

        RecyclerView recList = (RecyclerView) content.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        if (Utils.isTablet(getActivity())) {
            recList.setHasFixedSize(true);
            recList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
        SocialAdapter socialAdapter = new SocialAdapter(getActivity(), createList());
        recList.setAdapter(socialAdapter);


        return content;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<SobreInfo> createList() {

        List<SobreInfo> result = new ArrayList<SobreInfo>();

        SobreInfo linkedin = new SobreInfo();
        linkedin.setTitulo("Linkedin");
        linkedin.setInfo1("linkedin.com/in/joaoibarra");
        linkedin.setInfo2("Aqui você encontra todas minhas informações profissionais.");
        linkedin.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.joaolinkedin));
        result.add(linkedin);

        SobreInfo instagram = new SobreInfo();
        instagram.setTitulo("Instagram");
        instagram.setInfo1("instagram.com/joaoibarra");
        instagram.setInfo2("Aqui você descobre que sou um desenhista amador, praticante do esporte de bola ao cesto e de bets(em algumas regiões conhecido como taco, tacos ou bet).");
        instagram.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.instagram));
        result.add(instagram);

        SobreInfo facebook = new SobreInfo();
        facebook.setTitulo("Facebook");
        facebook.setInfo1("facebook.com/joao.ibarra");
        facebook.setInfo2("Aqui você percebe que não falo muito e que a maioria das postagens vem do Instagram XD");
        facebook.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.facebook));
        result.add(facebook);

        SobreInfo twitter = new SobreInfo();
        twitter.setTitulo("Twitter");
        twitter.setInfo1("twitter.com/joaoibarra");
        twitter.setInfo2("Não uso muito o twitter, mas estou lá.");
        twitter.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.jao));
        result.add(twitter);

        return result;
    }
}
