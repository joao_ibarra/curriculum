package com.joaoibarra.curriculo.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.SobreAdapter;
import com.joaoibarra.curriculo.models.SobreInfo;
import com.joaoibarra.curriculo.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */
public class FormacaoListFragment extends Fragment {
    public FormacaoListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View content = inflater.inflate(R.layout.fragment_sobre, container, false);

        RecyclerView recList = (RecyclerView) content.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        if (Utils.isTablet(getActivity())) {
            recList.setHasFixedSize(true);
            recList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
        SobreAdapter ca = new SobreAdapter(createList(30));
        recList.setAdapter(ca);

        FloatingActionButton myFab = (FloatingActionButton)  content.findViewById(R.id.add_button);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FrameLayout nav_contentframe = (FrameLayout) content.findViewById(R.id.nav_contentframe);
                Snackbar
                        .make(nav_contentframe, "Como explicado no sobre, este botão não faz nada :'(", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show();
            }
        });


        return content;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<SobreInfo> createList(int size) {

        List<SobreInfo> result = new ArrayList<SobreInfo>();
        SobreInfo ufms = new SobreInfo();
        ufms.setTitulo("Bacharel em Análise de Sistemas");
        ufms.setInfo1("UFMS");
        ufms.setInfo2("2006-2010");
        ufms.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.ufms));
        result.add(ufms);

        SobreInfo uniderp = new SobreInfo();
        uniderp.setTitulo("Engenharia da Computação");
        uniderp.setInfo1("UNIDERP");
        uniderp.setInfo2("2006-2009 (incompleto, parei no oitavo semestre)");
        uniderp.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.uniderp));
        result.add(uniderp);
        return result;
    }
}
