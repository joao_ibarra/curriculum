package com.joaoibarra.curriculo.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.SobreAdapter;
import com.joaoibarra.curriculo.models.SobreInfo;
import com.joaoibarra.curriculo.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */
public class SobreListFragment extends Fragment {

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SobreListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View content = inflater.inflate(R.layout.fragment_sobre, container, false);

        RecyclerView recList = (RecyclerView) content.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        //recList.addItemDecoration(new MarginDecoration(this));

        if (Utils.isTablet(getActivity())) {
            recList.setHasFixedSize(true);
            recList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }


        RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
            boolean hideToolBar = false;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (hideToolBar) {
                    ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
                } else {
                    ((AppCompatActivity)getActivity()).getSupportActionBar().show();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 20) {
                    hideToolBar = true;

                } else if (dy < -5) {
                    hideToolBar = false;
                }
            }
        };
        //recList.addOnScrollListener(onScrollListener);
        SobreAdapter ca = new SobreAdapter(createList(30));
        recList.setAdapter(ca);

        FloatingActionButton myFab = (FloatingActionButton)  content.findViewById(R.id.add_button);
        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FrameLayout nav_contentframe = (FrameLayout) content.findViewById(R.id.nav_contentframe);
                Snackbar
                        .make(nav_contentframe, "Este botão está aqui marotando. Talvez eu ache alguma função pra ele futuramente.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null)
                        .show();
            }
        });

        ImageView profilePic = (ImageView) content.findViewById(R.id.header_picture);

        String uri = "drawable/joao";

        int imageResource = getResources().getIdentifier(uri, null, getActivity().getPackageName());

        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), imageResource);


        RoundedBitmapDrawable roundedBitmapDrawable=
                RoundedBitmapDrawableFactory.create(getResources(), imageBitmap);
        roundedBitmapDrawable.setCornerRadius(1000.0f);
        roundedBitmapDrawable.setAntiAlias(true);
        profilePic.setImageDrawable(roundedBitmapDrawable);


        return content;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<SobreInfo> createList(int size) {

        List<SobreInfo> result = new ArrayList<SobreInfo>();
        SobreInfo nascimento = new SobreInfo();
        nascimento.setTitulo("Data de Nascimento");
        nascimento.setInfo1("27 de março de 1988");
        nascimento.setInfo2("");
        nascimento.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.me));
        result.add(nascimento);

        SobreInfo naturalidade = new SobreInfo();
        naturalidade.setTitulo("Naturalidade");
        naturalidade.setInfo1("Corumbá, MS (mas rioverdense de coração)");
        naturalidade.setInfo2("Brasil");
        naturalidade.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.porto_corumba));
        result.add(naturalidade);

        SobreInfo endereco = new SobreInfo();
        endereco.setTitulo("Endereço");
        endereco.setInfo1("Rua Londrina, 201, Panorama");
        endereco.setInfo2("Campo Grande, MS, 79044650");
        endereco.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.cgrande));
        result.add(endereco);

        SobreInfo contato = new SobreInfo();
        contato.setTitulo("Contato");
        contato.setInfo1("(67) 814552-54");
        contato.setInfo2("joao.ibarra@gmail.com");
        contato.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.cgrande));
        result.add(contato);

        return result;
    }
}
