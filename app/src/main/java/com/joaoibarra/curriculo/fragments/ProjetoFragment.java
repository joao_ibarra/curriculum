package com.joaoibarra.curriculo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.RecyclerAdapter;
import com.joaoibarra.curriculo.builders.ProjetoBuilder;
import com.joaoibarra.curriculo.models.ProjetoInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/25/15.
 */
public class ProjetoFragment extends Fragment {

    public final static String ID = "ID";

    public static ProjetoFragment createInstance(int id) {
        ProjetoFragment partThreeFragment = new ProjetoFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ID, id);
        partThreeFragment.setArguments(bundle);
        return partThreeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View relativeLayout = inflater.inflate(
                 R.layout.fragment_projeto, container, false);
        RecyclerView recyclerView = (RecyclerView) relativeLayout.findViewById(R.id.recyclerView);
        setupRecyclerView(recyclerView);
        return relativeLayout;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(createItemList());
        recyclerView.setAdapter(recyclerAdapter);
    }

    private List<ProjetoInfo> createItemList() {
        List<ProjetoInfo> projetoInfos = new ArrayList<>();
        int lol = getProjetoId();
        switch (getProjetoId()){
            case 0:
                //Modelo para melhorar
                ProjetoInfo eletrobras = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_eletrobras))
                        .setSubtitulo(getString(R.string.desenvolvedor_nastek))
                        .setDescricao(getString(R.string.descricao_eletrobras))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.eletrobras))
                        .createProjetoInfo();
                projetoInfos.add(eletrobras);

                eletrobras = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_eletrobras))
                        .setDescricao(getString(R.string.funcao_descricao_eletrobras))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(eletrobras);
                break;
            case 1:
                ProjetoInfo celpe = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_celpe))
                        .setSubtitulo(getString(R.string.desenvolvedor_nastek))
                        .setDescricao(getString(R.string.descricao_celpe))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.celpe))
                        .createProjetoInfo();
                projetoInfos.add(celpe);
                celpe = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_celpe))
                        .setDescricao(getString(R.string.funcao_descricao_celpe))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(celpe);
                break;
            case 2:
                ProjetoInfo cpfl = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_cpfl))
                        .setSubtitulo(getString(R.string.desenvolvedor_nastek))
                        .setDescricao(getString(R.string.descricao_cpfl))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cpfl))
                        .createProjetoInfo();
                        new ProjetoInfo();
                projetoInfos.add(cpfl);
                cpfl = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_cpfl))
                        .setDescricao(getString(R.string.funcao_descricao_cpfl))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(cpfl);
                break;
            case 3:
                ProjetoInfo pmesp = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_pmesp))
                        .setSubtitulo(getString(R.string.desenvolvedor_nastek))
                        .setDescricao(getString(R.string.descricao_pmesp))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.pmesp))
                        .createProjetoInfo();
                projetoInfos.add(pmesp);
                pmesp = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_pmesp))
                        .setDescricao(getString(R.string.funcao_descricao_pmesp))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(pmesp);
                break;
            case 4:
                ProjetoInfo gimpo = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_ui))
                        .setSubtitulo(getString(R.string.desenvolvedor_nastek))
                        .setDescricao(getString(R.string.descricao_gimpo))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.gimpo))
                        .createProjetoInfo();
                projetoInfos.add(gimpo);
                break;
            case 5:
                ProjetoInfo gerbov = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_boviplan))
                        .setSubtitulo(getString(R.string.desenvolvedor_catwork))
                        .setDescricao(getString(R.string.descricao_gerbov))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.gerbov))
                        .createProjetoInfo();
                projetoInfos.add(gerbov);
                gerbov =  new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_cakephp))
                        .setSubtitulo(getString(R.string.funcao_gerbov))
                        .setDescricao(getString(R.string.funcao_descricao_gerbov))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cake))
                        .createProjetoInfo();
                projetoInfos.add(gerbov);
                break;
            case 6:
                ProjetoInfo mercearia = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_mercearia))
                        .setSubtitulo(getString(R.string.desenvolvedor_catwork))
                        .setDescricao(getString(R.string.descricao_mercearia))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.mercearia))
                        .createProjetoInfo();
                projetoInfos.add(mercearia);
                mercearia = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_mercearia))
                        .setDescricao(getString(R.string.funcao_descricao_mercearia))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(mercearia);
                break;
            case 7:
                ProjetoInfo capins = new ProjetoBuilder()
                    .setTitulo(getString(R.string.projeto_capins))
                    .setSubtitulo(getString(R.string.desenvolvedor_catwork))
                    .setDescricao(getString(R.string.descricao_capins))
                    .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.capins))
                    .createProjetoInfo();
                projetoInfos.add(capins);
                capins = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_capins))
                        .setDescricao(getString(R.string.funcao_descricao_capins))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(capins);
                break;
            case 8:
                ProjetoInfo scot = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_scot))
                        .setSubtitulo(getString(R.string.desenvolvedor_catwork))
                        .setDescricao(getString(R.string.descricao_scot))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.scot))
                        .createProjetoInfo();
                projetoInfos.add(scot);
                scot = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_scot))
                        .setDescricao(getString(R.string.funcao_descricao_scot))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(scot);
                break;
            case 9:
                ProjetoInfo ipam = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_ipam))
                        .setSubtitulo(getString(R.string.desenvolvedor_agence))
                        .setDescricao(getString(R.string.descricao_ipam))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.ipam))
                        .createProjetoInfo();
                projetoInfos.add(ipam);
                ipam = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_symfony))
                        .setSubtitulo(getString(R.string.funcao_ipam))
                        .setDescricao(getString(R.string.funcao_descricao_ipam))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.symfony))
                        .createProjetoInfo();
                projetoInfos.add(ipam);
                break;
            case 10:
                ProjetoInfo luigi = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_luigi))
                        .setSubtitulo(getString(R.string.desenvolvedor_agence))
                        .setDescricao(getString(R.string.descricao_luigi))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.luigi))
                        .createProjetoInfo();
                projetoInfos.add(luigi);
                luigi = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_cakephp))
                        .setSubtitulo(getString(R.string.funcao_luigi))
                        .setDescricao(getString(R.string.funcao_descricao_luigi))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cake))
                        .createProjetoInfo();
                projetoInfos.add(luigi);
                break;
            case 11:
                ProjetoInfo santa = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_santa_graca))
                        .setSubtitulo(getString(R.string.desenvolvedor_agence))
                        .setDescricao(getString(R.string.descricao_santa_graca))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.santa))
                        .createProjetoInfo();
                projetoInfos.add(santa);
                santa = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_cakephp))
                        .setSubtitulo(getString(R.string.funcao_santa_graca))
                        .setDescricao(getString(R.string.funcao_descricao_santa_graca))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cake))
                        .createProjetoInfo();
                projetoInfos.add(santa);
                break;
            case 12:
                ProjetoInfo paulistinha = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_paulistinha))
                        .setSubtitulo(getString(R.string.desenvolvedor_agence))
                        .setDescricao(getString(R.string.descricao_paulistinha))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.paulistinha))
                        .createProjetoInfo();
                projetoInfos.add(paulistinha);
                paulistinha = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_cakephp))
                        .setSubtitulo(getString(R.string.funcao_paulistinha))
                        .setDescricao(getString(R.string.funcao_descricao_paulistinha))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cake))
                        .createProjetoInfo();
                projetoInfos.add(paulistinha);
                break;
            case 13:
                ProjetoInfo akad = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_akad))
                        .setSubtitulo(getString(R.string.desenvolvedor_agence))
                        .setDescricao(getString(R.string.descricao_akad))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.akad))
                        .createProjetoInfo();
                projetoInfos.add(akad);
                akad = new ProjetoBuilder()
                    .setTitulo(getString(R.string.plataforma_wordpress))
                    .setSubtitulo(getString(R.string.funcao_akad))
                    .setDescricao(getString(R.string.funcao_descricao_akad))
                    .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.wordpress))
                    .createProjetoInfo();
                projetoInfos.add(akad);
                break;
            case 14:
                ProjetoInfo smart = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_smartfactor))
                        .setSubtitulo(getString(R.string.desenvolvedor_nds))
                        .setDescricao(getString(R.string.descricao_smartfactor))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.smartfactor))
                        .createProjetoInfo();
                projetoInfos.add(smart);
                smart = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_php))
                        .setSubtitulo(getString(R.string.funcao_smartfactor))
                        .setDescricao(getString(R.string.funcao_descricao_smartfactor))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.php))
                        .createProjetoInfo();
                projetoInfos.add(smart);
                break;
            case 15:
                ProjetoInfo estudo = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_estudos))
                        .setSubtitulo(getString(R.string.desenvolvedor_nds))
                        .setDescricao(getString(R.string.descricao_estudo))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.smartfactor))
                        .createProjetoInfo();
                projetoInfos.add(estudo);
                estudo = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_varias))
                        .setSubtitulo(getString(R.string.funcao_estudo))
                        .setDescricao(getString(R.string.funcao_descricao_estudo))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.smartfactor))
                        .createProjetoInfo();
                projetoInfos.add(estudo);
                break;
            case 16:
                ProjetoInfo cg115 = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_cg115))
                        .setSubtitulo(getString(R.string.desenvolvedor_joao_catwork))
                        .setDescricao(getString(R.string.descricao_cg115))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cg115))
                        .createProjetoInfo();
                projetoInfos.add(cg115);
                cg115 = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_android))
                        .setSubtitulo(getString(R.string.funcao_cg115))
                        .setDescricao(getString(R.string.funcao_descricao_cg115))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.android))
                        .createProjetoInfo();
                projetoInfos.add(cg115);
                break;
            case 17:
                ProjetoInfo postal = new ProjetoBuilder()
                        .setTitulo(getString(R.string.projeto_postal_saude))
                        .setSubtitulo(getString(R.string.desenvolvedor_dazopi))
                        .setDescricao(getString(R.string.descricao_postal_saude))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.postal))
                        .createProjetoInfo();
                projetoInfos.add(postal);
                postal = new ProjetoBuilder()
                        .setTitulo(getString(R.string.plataforma_cakephp))
                        .setSubtitulo(getString(R.string.funcao_postal_saude))
                        .setDescricao(getString(R.string.funcao_descricao_postal_saude))
                        .setFoto(ContextCompat.getDrawable(getContext(), R.drawable.cake))
                        .createProjetoInfo();
                projetoInfos.add(postal);
                break;
        }


        return projetoInfos;
    }

    public int getProjetoId() {
        return getArguments().getInt("ID", 0);
    }
}
