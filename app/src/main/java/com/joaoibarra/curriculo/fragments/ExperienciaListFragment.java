package com.joaoibarra.curriculo.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.joaoibarra.curriculo.R;
import com.joaoibarra.curriculo.adapters.ClassificacaoAdapter;
import com.joaoibarra.curriculo.adapters.ExperienciaAdapter;
import com.joaoibarra.curriculo.adapters.SobreAdapter;
import com.joaoibarra.curriculo.models.ExperienciaInfo;
import com.joaoibarra.curriculo.models.SobreInfo;
import com.joaoibarra.curriculo.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaoibarra on 6/18/15.
 */
public class ExperienciaListFragment extends Fragment {
    public ExperienciaListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_list_classificacao, container, false);

        RecyclerView recList = (RecyclerView) content.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        if (Utils.isTablet(getActivity())) {
            recList.setHasFixedSize(true);
            recList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
        ExperienciaAdapter ca = new ExperienciaAdapter(createList(30), getActivity());
        recList.setAdapter(ca);


        return content;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private List<ExperienciaInfo> createList(int size) {

        List<ExperienciaInfo> result = new ArrayList<ExperienciaInfo>();
        ExperienciaInfo nastek = new ExperienciaInfo();
        nastek.setTitulo("Desenvolvedor Android Pleno");
        nastek.setInfo1("Nastek Indústria e Tecnologia");
        nastek.setInfo2("08/2014 - Atualmente");
        nastek.setInfo3("Foco na experiência do usuário, principalmente " +
                "na parte de layout. Localização utilizando mapas " +
                "offline (OpenStreetMaps, Google Maps e Sygic). " +
                "Desenvolvimento e manutenção de sistemas de " +
                "localização e comunicação de viaturas.");
        nastek.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.nastek));
        result.add(nastek);

        ExperienciaInfo catwork = new ExperienciaInfo();
        catwork.setTitulo("Desenvolvedor Web/Mobile");
        catwork.setInfo1("Catwork Tecnologia");
        catwork.setInfo2("02/2011-08/2014");
        catwork.setInfo3("esenvolvimento de sistemas web utilizando" +
                "Cakephp," +
                "Bootstrap, " +
                "Mysql " +
                "e " +
                "Jquery. " +
                "Desenvolvimento de aplicativos para Android");
        catwork.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.catwork));
        result.add(catwork);

        ExperienciaInfo agence = new ExperienciaInfo();
        agence.setTitulo("Desenvolvedor Web");
        agence.setInfo1("Agence Consultoria");
        agence.setInfo2("08/2010-02/2011");
        agence.setInfo3("Desenvolvimento de sistemas web utilizando " +
                "Cakephp, Symfony, Wordpress e Jquery.");
        agence.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.agence));
        result.add(agence);

        ExperienciaInfo nds = new ExperienciaInfo();
        nds.setTitulo("Desenvolvedor Web estagiário");
        nds.setInfo1("NDS Tecnologia");
        nds.setInfo2("05/2010 - 08/2010");
        nds.setInfo3("Manutenção de sistemas em PHP, Ajax e " +
                "Javascript. Estudos em Java (Jsf, Hibernate e " +
                "RichFaces), Asp.net e C#, PHP e Doctrine, " +
                "Cakephp.");
        nds.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.nds));
        result.add(nds);

        ExperienciaInfo freelance = new ExperienciaInfo();
        freelance.setTitulo("Desenvolvedor Web/Mobile");
        freelance.setInfo1("João Ibarra");
        freelance.setInfo2("07/2014 - Atualmente");
        freelance.setInfo3("Desenvolvimento de aplicativos para Android e sites.");
        freelance.setFoto(ContextCompat.getDrawable(getActivity(), R.drawable.icon_app));
        result.add(freelance);

        return result;
    }
}
