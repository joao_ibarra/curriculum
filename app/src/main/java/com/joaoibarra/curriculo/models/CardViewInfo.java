package com.joaoibarra.curriculo.models;

/**
 * Created by joaoibarra on 2/23/15.
 */
public class CardViewInfo{
    public String name;
    public String surname;
    public String email;
    public static final String NAME_PREFIX = "Name_";
    public static final String SURNAME_PREFIX = "Surname_";
    public static final String EMAIL_PREFIX = "email_";
}