package com.joaoibarra.curriculo.models;

import android.graphics.drawable.Drawable;

/**
 * Created by joaoibarra on 6/18/15.
 */
public class SobreInfo {
    protected String titulo;
    protected String info1;
    protected String info2;
    protected Drawable foto;


    public static final String NAME_PREFIX = "J.";
    public static final String SURNAME_PREFIX = "IBARRA";
    public static final String EMAIL_PREFIX = "12";

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getInfo1() {
        return info1;
    }

    public void setInfo1(String info1) {
        this.info1 = info1;
    }

    public String getInfo2() {
        return info2;
    }

    public void setInfo2(String info2) {
        this.info2 = info2;
    }

    public Drawable getFoto() {
        return foto;
    }

    public void setFoto(Drawable foto) {
        this.foto = foto;
    }
}

