package com.joaoibarra.curriculo.models;

import android.graphics.drawable.Drawable;

/**
 * Created by joaoibarra on 6/26/15.
 */
public class ProjetoInfo {
    protected String titulo;
    protected String subtitulo;
    protected String descricao;
    protected Drawable foto;


    public static final String NAME_PREFIX = "J.";
    public static final String SURNAME_PREFIX = "IBARRA";
    public static final String EMAIL_PREFIX = "12";

    public ProjetoInfo() {
    }

    public ProjetoInfo(String titulo, String subtitulo, String descricao, Drawable foto) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.descricao = descricao;
        this.foto = foto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Drawable getFoto() {
        return foto;
    }

    public void setFoto(Drawable foto) {
        this.foto = foto;
    }
}
